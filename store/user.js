import {defineStore} from "pinia";

export const userStore = defineStore("user", () => {
    const user = reactive({
        username: "",
        email: "",
        accessToken: "",
        avatar: ""
    })
})