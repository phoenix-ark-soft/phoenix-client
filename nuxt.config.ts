// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    devtools: { enabled: true },
    css: [
        "vuetify/lib/styles/main.sass",
        "@mdi/font/css/materialdesignicons.min.css",
        "~/assets/styles/main.scss",
    ],
    build: {
        transpile: ["vuetify"]
    },
    vite: {
        vue: {
            customElement: true
        },
        vueJsx: {
            mergeProps: true
        }
    },
    modules: [
        '@pinia/nuxt',
    ]
})
